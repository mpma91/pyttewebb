-------------------------------------------------------------------------------
PytteWebb
-------------------------------------------------------------------------------
Version 1.0
Release date: 02/27/2014
-------------------------------------------------------------------------------
Author
	Mathias Andreasen (ma222ce@student.lnu.se)
-------------------------------------------------------------------------------
Project description

Pyttewebb is a simple web server that handles each new connection
in a seperate thread. The Server runs on the HTTP/1.1 protocol

This project is part of the first assignment of the course 
"Datakommunikation för Programmerare" given by Uppsala University.
-------------------------------------------------------------------------------
Usage

java -jar PytteWebb.jar <port>

if no arguments are given the server will listen to port 8080 by default
-------------------------------------------------------------------------------
package pyttewebb;

import java.io.IOException;

import org.omg.SendingContext.RunTime;

import pyttewebb.server.PytteServer;

/**
 * PytteWebb.java (UTF-8)
 *
 * Program entry point.
 *
 * 2014-jan-28
 * @author Mathias Andreasen <ma222ce@student.lnu.se>
 */
public class PytteWebb {

    /**
     * Usage: java PytteWebb
     *
     * @params args port to listen to
     */
    public static void main(String[] args) {
        int port = 8080;

        if(args.length > 0)
            port = Integer.valueOf(args[0]);

        PytteServer server = new PytteServer(port);
        System.out.println("Starting server...");

        try {
            server.runServer();
        } catch(IOException e) {
            System.err.println("Could not start the server\n" + e.getMessage());
        }
    }
}

package pyttewebb.server;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import pyttewebb.server.request.Handler;

/**
 * PytteServer.java (UTF-8)
 *
 * PytteWebb is a simple multi-threaded web server. The server is multithreaded
 * in order to serve multiple clients in parallel.
 *
 * 2014-jan-21
 * @author Mathias Andreasen <ma222ce@student.lnu.se>
 */
public class PytteServer {

    private final int PORT;

    /**
     * Constructs a PytteServer object that listens to a selected port
     *
     * @param port the server will isten on.
     */
    public PytteServer(int port) {
        PORT = port;
    }

    /**
     * Starts the server and starts each new connection in a different thread
     * in order to serve multiple connections in parallel.
     *
     * The server is run within a while loop that will run indefinitely, this
     * means however that in order to close the server we must invoke the
     * process SIGTERM by pressing ^D (CTRL+D) in the server terminal window.
     *
     * @throws IOException if we couldn't open the server port
     */
    public void runServer() throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        System.out.println("\nPytteWebb running on port: "
                + PORT + "\nwaiting for connections...");

        while(true) {
            Socket client = server.accept();
            System.out.println("\nClient connected: " + client.getInetAddress()
                    + ":" + client.getPort());

            // send client connection to a seperate thread
            new Thread(new Handler(client)).start();
        }
    }
}

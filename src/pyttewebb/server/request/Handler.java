package pyttewebb.server.request;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Handler.java (UTF-8)
 *
 * Handles HTTP requests sent by the client.
 *
 * @author Mathias Andreasen <ma222ce@student.lnu.se>
 */
public class Handler implements Runnable {

    private         Socket              socket;
    private         Request             request;
    private         Parser              parser;

    private final   InputStream         IN;
    private final   DataOutputStream    OUT;

    /**
     * @param socket - client connection to handle
     * @throws IOException if we could not handle the request
     */
    public Handler(Socket socket) throws IOException {
        this.socket     = socket;
        IN              = socket.getInputStream();
        OUT             = new DataOutputStream(socket.getOutputStream());
        parser          = new Parser(IN);
    }

    @Override
    public void run() {
        try {
            String[] req = parser.parseRequest();
            request = new Request(req[0], req[1], req[2]);
            handleRequest(request);
        } catch(IOException e) {
            System.err.println("Couldn't handle request");
        }
    }

    /**
     * Handles the request according to what type of request it is.
     *
     * @param request to be handled
     * @throws IOException if we could not handle the request
     */
    public void handleRequest(Request request) throws IOException {
        FileInputStream fis         = null;
        Response response           = null;
        String          status      = "";
        int             length      = 0;

        if(request.isValid()) {
            // does the file exist on our server?
            try {
                fis = new FileInputStream("." + request.getResource());
                status = "200 OK";
                length = fis.available();
            } catch(IOException e) {
                fis = new FileInputStream("404.html");
                status  = "404 Not Found";
                length = fis.available();
            }

            response = new Response(status, getType(request.getResource()), length);

            if(request.isGET()) {
                OUT.writeBytes(response.getResponse());
                send(fis, OUT);
            }
            else {
                OUT.writeBytes(response.getResponse());
            }
        }
        // NOT VALID HTTP REQUEST
        else {
            fis     = new FileInputStream("bad.html");
            status  = "400 Bad Request";
            length  = fis.available();

            response = new Response(status, getType("bad.html"), length);
            OUT.writeBytes(response.getResponse());
            send(fis, OUT);
        }

        // clean up
        dispose();
    }

    /**
     * @return content-type of the requested resource
     */
    public String getType(String resource) {
        if(resource.matches("([^\\s]+(\\.(?i)(html|HTML|htm|HTM))$)"))
            return "Content-Type: text/html";
        else if(resource.matches("([^\\s]+(\\.(?i)(jpg|JPG|jpeg|JPEG))$)"))
            return "Content-Type: image/jpeg";
        else if(resource.matches("([^\\s]+(\\.(?i)(gif|GIF|))$)"))
            return "Content-Type: image/gif";
        else if(resource.matches("([^\\s]+(\\.(?i)(png|PNG))$)"))
            return "Content-Type: image/png";
        else if(resource.matches("([^\\s]+(\\.(?i)(txt|TXT))$)"))
            return "Content-Type: text/plain";

        else
            return "Content-Type: UNSUPPORTED";
    }

    /**
     * Sends the resource to the client.
     *
     * @param fis - resource to be sent
     * @param out - medium to send it over
     * @throws IOException if we couldn't read/send the resource
     */
    private void send(FileInputStream fis, OutputStream out) throws IOException {
        byte[] buf = new byte[1024];
        int b;
        while((b = fis.read(buf)) != -1)
            out.write(buf, 0, b);
    }

    private void dispose() {
        try {
            OUT.close();
            socket.close();
            IN.close();
        } catch(IOException e) {
            System.exit(0);
        }
    }
}

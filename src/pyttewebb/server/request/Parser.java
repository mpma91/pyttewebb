package pyttewebb.server.request;

import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;

/**
 * Parser.java (UTF-8)
 *
 * Parses client request sent to the server.
 *
 * @author Mathias Andreasen <ma222ce@student.lnu.se
 */
public class Parser {

    private static final String CRLF = "\r\n"; // carrige-return, line-feed

    private InputStream         in;

    /**
     * @param in - inputstream to read from
     */
    public Parser(InputStream in) {
        this.in = in;
    }

    /**
     * Parses client request and returns a string array with the result.
     * @return string array [0] = request, [1] = path, [2] = protocol
     */
    public String[] parseRequest() throws IOException {
        String      received    = "";
        String[]    request     = new String[3];

        try {
            received = read(this.in);
        } catch(IOException e) {
            System.err.println("Request could not be parsed");
            System.exit(0);
        }

        StringTokenizer tokens = new StringTokenizer(received);

        // looks messy, but get's the job done
        if(tokens.countTokens() == 0)
            request[0] = request[1] = request[2] = "BAD";
        else if(tokens.countTokens() == 1 && tokens.nextToken().matches("/quit|/q|/QUIT|/Q")) {
            System.out.println("[WARNING]: Server shutting down");
            System.exit(0);
        }
        else if(tokens.countTokens() < 3)
            request[0] = request[1] = request[2] = "BAD";
        else {
            for(int i = 0; i < 3; i++)
                request[i] = tokens.nextToken();

            // fix the path if necessary
            if(request[1].equals("/"))
                request[1] = "/index.html";
        }

        return request;
    }

    // reads from the inputstream and returns as string
    private String read(InputStream in) throws IOException {
        String      r           = "";
        int         available   = 0;
        byte[]      bytes       = new byte[available];

        while(!r.endsWith(CRLF)) {
            available = in.available();

            bytes = new byte[available];
            this.in.read(bytes);

            r += new String(bytes);
        }

        return r;
    }

}

package pyttewebb.server.request;

/**
 * Request.java (UTF-8)
 *
 * In charge of constructing and validate HTTP requests.
 *
 * @author Mathias Andreasen <ma222ce@student.lnu.se>
 */
public class Request {

    private String httpRequest;
    private String resource;
    private String protocol;

    public Request() {}

    /**
     * @param httpRequest - HTTP request, eg GET/HEAD
     * @param resource - requested resource
     * @param protocol - HTTP protocol to be used
     */
    public Request(String httpRequest, String resource, String protocol) {
        this.httpRequest    = httpRequest;
        this.resource       = resource;
        this.protocol       = protocol;
    }

    /**
     * @return true if request is GET
     */
    public boolean isGET() {
        return isValid() == this.httpRequest.matches("GET|get");
    }

    /**
     * @return true if request is HEAD
     */
    public boolean isHEAD() {
        return isValid() == this.httpRequest.matches("HEAD|head");
    }

    public String getHttpRequest() { return this.httpRequest; }
    public String getResource() { return this.resource; }
    public String getProtocol() { return this.protocol; }

    /**
     * @return true if request is a valid HTTP/1.1 request
     */
    public boolean isValid() {
        return this.httpRequest.matches("GET|get|HEAD|head")
            && this.protocol.matches("HTTP/1.1|http/1.1");
    }

}

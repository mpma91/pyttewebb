package pyttewebb.server.request;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Response {

    private String  status;
    private String  type;
    private int     length;

    /**
     * Creates a HTTP/1.1 response.
     *
     * @param status - status header
     * @param type - content-type
     * @param length - content-length
     */
    public Response(String status, String type, int length) {
        this.status = status;
        this.type   = type;
        this.length = length;
    }

    /**
     * @return HTTP/1.1 response
     */
    public String getResponse() {
        String CRLF = "\r\n";
        String r    = "HTTP/1.1 ";

        r += this.status;
        r += CRLF;

        r += "Date: " + new SimpleDateFormat("EEE d MMM yyy HH:mm:ss z")
            .format(new Date());
        r += CRLF;

        r += this.type;
        r += CRLF;

        r += "Content-Length: " + this.length;
        r += CRLF;

        r += CRLF;
        return r;
    }

}
